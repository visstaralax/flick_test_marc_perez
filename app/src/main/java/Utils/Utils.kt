package Utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities

class Utils internal constructor(private val context: Context){

    fun getString(text: Int): String{
        return context.getString(text)
    }

    fun isNetworkAvailable() =
        (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run {
            getNetworkCapabilities(activeNetwork)?.run {
                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                        || hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                        || hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            } ?: false
        }
}
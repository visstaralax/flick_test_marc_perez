package com.nextnetwork.ApiModel

import com.nextnetwork.ApiModel.FlickrPhotoDetailResponse.FlickrStateDetailResponse
import com.nextnetwork.ApiModel.FlickrPhotoResponse.FlickrPagesResponse
import com.nextnetwork.ApiModel.FlickrPhotoResponse.FlickrStateResponse
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url


interface APIService {

    @GET //"users/{username}"
    fun getStateResponse(@Url url: String): Call<FlickrStateResponse?>?

    @GET
    fun getPhotos(@Url url: String): Call<FlickrPagesResponse?>?

    @GET
    fun getPhoto(): Call<FlickrPhoto>

    @GET
    fun getPhotoDetail(@Url url: String): Call<FlickrStateDetailResponse>



}
package com.nextnetwork.ApiModel

enum class FlickMethodAPIService  {
    SEARCH,
    GETINFO
}
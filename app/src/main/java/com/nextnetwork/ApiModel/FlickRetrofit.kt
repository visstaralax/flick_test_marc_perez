package com.nextnetwork.ApiModel

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import root.Root


class FlickRetrofit {

    companion object {

        val BASE_URL = Root.appInfo.urlBase
        var retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        fun getAPIService(): APIService {

            return retrofit.create(APIService::class.java)
        }
    }
}
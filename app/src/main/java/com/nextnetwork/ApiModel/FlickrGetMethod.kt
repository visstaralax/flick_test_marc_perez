package com.nextnetwork.ApiModel

class FlickrGetMethod {

    fun getMethod(method: FlickMethodAPIService): String {

        when (method) {

            FlickMethodAPIService.SEARCH -> {
                return "flickr.photos.search"
            }

            FlickMethodAPIService.GETINFO -> {
                return "flickr.photos.getInfo"
            }

            else -> {return ""}
        }
    }
}
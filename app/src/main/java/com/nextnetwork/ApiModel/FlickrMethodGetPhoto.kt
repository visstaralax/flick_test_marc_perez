package com.nextnetwork.ApiModel

enum class FlickrMethodGetPhoto {
    ICON,
    IMAGE,
    SEARCH
}
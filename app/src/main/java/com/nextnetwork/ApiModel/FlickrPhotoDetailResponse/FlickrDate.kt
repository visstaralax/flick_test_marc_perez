package com.nextnetwork.ApiModel.FlickrPhotoDetailResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FlickrDate {
    @SerializedName("posted")
    @Expose
    var posted: String? = null

    @SerializedName("taken")
    @Expose
    var taken: String? = null

    @SerializedName("takengranularity")
    @Expose
    var takengranularity: String? = null

    @SerializedName("takenunknown")
    @Expose
    var takenunknown: String? = null

    @SerializedName("lastupdate")
    @Expose
    var lastupdate: String? = null
}
package com.nextnetwork.ApiModel.FlickrPhotoDetailResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FlickrDescription {
    @SerializedName("_content")
    @Expose
    var content: String? = null
}
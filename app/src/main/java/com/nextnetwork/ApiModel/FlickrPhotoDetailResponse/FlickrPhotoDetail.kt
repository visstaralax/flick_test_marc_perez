package com.nextnetwork.ApiModel.FlickrPhotoDetailResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FlickrPhotoDetail {
    @SerializedName("id")
    @Expose
    var id: String = ""

    @SerializedName("secret")
    @Expose
    var secret: String = ""

    @SerializedName("server")
    @Expose
    var server: String = ""

    @SerializedName("farm")
    @Expose
    var farm = 0

    @SerializedName("dateuploaded")
    @Expose
    var dateuploaded: String = ""

    @SerializedName("isfavorite")
    @Expose
    var isfavorite = 0

    @SerializedName("license")
    @Expose
    var license: String = ""

    @SerializedName("safety_level")
    @Expose
    var safetyLevel: String = ""

    @SerializedName("rotation")
    @Expose
    var rotation = 0

    @SerializedName("originalsecret")
    @Expose
    var originalsecret: String = ""

    @SerializedName("originalformat")
    @Expose
    var originalformat: String = ""

    @SerializedName("owner")
    @Expose
    var flickrOwner: FlickrOwner? = null

    @SerializedName("title")
    @Expose
    var title: FlickrTitle? = null

    @SerializedName("description")
    @Expose
    var description: FlickrDescription? = null

    //@SerializedName("visibility")
    //@Expose
    //var visibility: Visibility? = null

    @SerializedName("dates")
    @Expose
    var dates: FlickrDate? = null

    @SerializedName("views")
    @Expose
    var views: String? = null

    //@SerializedName("editability")
    //@Expose
    //var editability: Editability? = null

    //@SerializedName("publiceditability")
    //@Expose
    //var publiceditability: Publiceditability? = null

    /*
    @SerializedName("usage")
    @Expose
    var usage: Usage? = null


    @SerializedName("comments")
    @Expose
    var comments: Comments? = null

    @SerializedName("notes")
    @Expose
    var notes: Notes? = null

    @SerializedName("people")
    @Expose
    var people: People? = null

    @SerializedName("tags")
    @Expose
    var tags: Tags? = null

    @SerializedName("urls")
    @Expose
    var urls: Urls? = null

    @SerializedName("media")
    @Expose
    var media: String? = null

    */
}
package com.nextnetwork.ApiModel.FlickrPhotoDetailResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.nextnetwork.ApiModel.FlickrPhotoResponse.FlickrPagesResponse

data class FlickrStateDetailResponse (
    @SerializedName("stat") @Expose var status:String,
    @SerializedName("photo") @Expose var images: FlickrPhotoDetail
)
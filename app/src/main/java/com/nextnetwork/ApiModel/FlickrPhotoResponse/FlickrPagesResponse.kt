package com.nextnetwork.ApiModel.FlickrPhotoResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName;
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto

data class FlickrPagesResponse(
    @SerializedName("page") @Expose var page: Int,
    @SerializedName("pages") @Expose var pages: Int,
    @SerializedName("perpage") @Expose var perpage: Int,
    @SerializedName("total") @Expose var total: String,
    @SerializedName("photo") @Expose var photo:List<FlickrPhoto>? = null
) {

    fun ishigherPagination(): Boolean {
        return (page <= pages)
    }

}

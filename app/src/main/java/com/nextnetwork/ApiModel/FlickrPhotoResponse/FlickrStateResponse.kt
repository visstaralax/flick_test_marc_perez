package com.nextnetwork.ApiModel.FlickrPhotoResponse
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName;

data class FlickrStateResponse (
    @SerializedName("stat") @Expose var status:String,
    @SerializedName("photos") @Expose var images: FlickrPagesResponse
)
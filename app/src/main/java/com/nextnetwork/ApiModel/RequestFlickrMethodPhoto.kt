package com.nextnetwork.ApiModel

import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto

class RequestFlickrMethodPhoto {

    fun requestBase(getPhoto: FlickrMethodGetPhoto, flickPhoto: FlickrPhoto): String {

        return when (getPhoto) {

            FlickrMethodGetPhoto.ICON -> ("https://live.staticflickr.com/${flickPhoto.server}/${flickPhoto.id}_${flickPhoto.secret}_s.jpg")
            FlickrMethodGetPhoto.IMAGE -> ("https://live.staticflickr.com/${flickPhoto.server}/${flickPhoto.id}_${flickPhoto.secret}.jpg")
            else -> {""}
        }
    }
}
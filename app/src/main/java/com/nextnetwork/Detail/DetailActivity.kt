package com.nextnetwork.Detail

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.facebook.drawee.view.SimpleDraweeView
import com.nextnetwork.ApiModel.*
import com.nextnetwork.ApiModel.FlickrPhotoDetailResponse.FlickrStateDetailResponse
import com.nextnetwork.R
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto
import root.FlickrActivity
import root.Root

class DetailActivity : FlickrActivity() {

    //por la poca información de la clase y la actividad he considerado no hacer el patrón MVP

    private lateinit var api: APIService
    private val flickrPhotoUrl = RequestFlickrMethodPhoto()
    private val flickrServiceAPI = FlickrGetMethod()
    private lateinit var context: Context

    private lateinit var flickrPhoto: FlickrPhoto
    private lateinit var img: SimpleDraweeView
    private lateinit var progressBar: ProgressBar

    private lateinit var title: TextView
    private lateinit var description: TextView
    private lateinit var author: TextView
    private lateinit var authorInfo: TextView
    private lateinit var date: TextView
    private lateinit var dateInfo: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.photo_activity)
        context = this

        if (!checkInternetConnection(context)) {
            Toast.makeText(context, R.string.no_internet, Toast.LENGTH_LONG).show()
            finish()
        }

        setToolbar(R.id.toolbar, R.string.photo_details, true)
        api = FlickRetrofit.getAPIService()

        getExtra()
        setViews()
        getInfo()
    }

    private fun getExtra() {
        if (intent.hasExtra("flickr_photo"))
            flickrPhoto = intent.getSerializableExtra("flickr_photo") as FlickrPhoto
    }

    private fun setViews() {
        title = findViewById(R.id.txt_title)
        description = findViewById(R.id.txt_des)

        author = findViewById(R.id.txt_author)
        authorInfo = findViewById(R.id.txt_author_info)

        date = findViewById(R.id.txt_date)
        dateInfo = findViewById(R.id.txt_date_info)

        progressBar = findViewById(R.id.progressBar)
        img = findViewById(R.id.img2)

        img.setImageURI(flickrPhotoUrl.requestBase(FlickrMethodGetPhoto.IMAGE, flickrPhoto))
    }

    private fun getInfo() {

        val method = flickrServiceAPI.getMethod(FlickMethodAPIService.GETINFO)
        val url = "${Root.appInfo.urlBase}services/rest/?method=$method&api_key=${Root.appInfo.flickrKey}&photo_id=${flickrPhoto.id}&secret=${flickrPhoto.secret}&format=json&nojsoncallback=1"
        val call = api.getPhotoDetail(url)

        call.enqueue(object : retrofit2.Callback<FlickrStateDetailResponse?> {
            override fun onResponse(
                call: retrofit2.Call<FlickrStateDetailResponse?>,
                response: retrofit2.Response<FlickrStateDetailResponse?>) {
                if (response.body() != null)

                    if (response.body()!!.status == "ok") {
                        title.text = response.body()!!.images.title!!.content
                        authorInfo.text = response.body()!!.images.flickrOwner!!.username
                        dateInfo.text = response.body()!!.images.dates!!.taken

                        if (response.body()!!.images.description!!.content!!.isNotEmpty())
                            description.text = response.body()!!.images.description!!.content
                        else
                            description.text = getString(R.string.description_not_content)
                    }

                progressBar.visibility = View.INVISIBLE
            }

            override fun onFailure(call: retrofit2.Call<FlickrStateDetailResponse?>, t: Throwable) {
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    Toast.makeText(this@DetailActivity, R.string.try_again, Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}
package com.nextnetwork.Search

import SearchAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nextnetwork.Detail.DetailActivity
import com.nextnetwork.R
import com.nextnetwork.Search.model.FilterType
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto
import root.FlickrActivity


class MainActivity : FlickrActivity(), MainActivityMVP.View {

    private var internet = true

    private lateinit var context: Context
    private lateinit var presenter: MainActivityMVP.Presenter
    private lateinit var adapter: SearchAdapter
    private lateinit var layoutNotResults: LinearLayout
    private lateinit var layoutStartIndications: LinearLayout

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var btnFilterTags: Button
    private lateinit var btnFilterLocation: Button
    private lateinit var btnFilterDate: Button
    private lateinit var btnFilterSort: Button
    private lateinit var btnSubmitText: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this

        setPresenter()
        setToolbar(R.id.toolbar, R.string.flickr_search, false)
        setViews()
        setClickListeners()
        setRecyclerView(presenter)
    }

    private fun setPresenter() {
        presenter = MainActivityPresenter(context, this)
    }

    private fun setViews() {
        recyclerView = findViewById(R.id.recycler)
        progressBar = findViewById(R.id.progressbar)
        layoutNotResults = findViewById(R.id.notResults)
        layoutStartIndications = findViewById(R.id.start_indications)

        btnFilterTags = findViewById(R.id.filter_tags)
        btnFilterLocation = findViewById(R.id.filter_location)
        btnFilterDate = findViewById(R.id.filter_time)
        btnFilterSort = findViewById(R.id.filter_sort)
    }

    private fun setRecyclerView(presenter: MainActivityMVP.Presenter) {

        adapter = SearchAdapter(presenter)

        recyclerView = findViewById(R.id.recycler)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

    }

    private fun setClickListeners() {
        btnFilterSort.setOnClickListener {
            presenter.btnFilterHasClicked(FilterType.SORT)
        }

        btnFilterDate.setOnClickListener {
            presenter.btnFilterHasClicked(FilterType.DATE)
        }

        btnFilterLocation.setOnClickListener {
            presenter.btnFilterHasClicked(FilterType.LOCATION)
        }

        btnFilterTags.setOnClickListener {
            presenter.btnFilterHasClicked(FilterType.TAG)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        btnSubmitText = menu.findItem(R.id.action_search)
        val searchView = btnSubmitText.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String): Boolean {

                presenter.onQueryTextSubmit(query)
                return false
            }

            override fun onQueryTextChange(s: String?): Boolean {
                return false
            }
        })
        return true
    }

    override fun updateAdapter(listPhotos: List<FlickrPhoto>) {
        adapter.updateList(listPhotos)
    }

    override fun intentToDetailActivity(itemInfo: FlickrPhoto?, putExtra: Boolean) {
        val intent = Intent(context, DetailActivity::class.java)

        if (putExtra)
            intent.putExtra("flickr_photo", itemInfo)

        startActivity(intent)
    }

    override fun setProgressBar(view: Int) {
        progressBar.visibility = view
    }

    override fun collapseBtnSubmitText() {
        btnSubmitText.collapseActionView()
    }

    override fun showLocationFilterAlertDialog(checkOptionsList: BooleanArray, nameOptionsList: Array<String>, filterType: FilterType) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.location_filter)

        builder.setMultiChoiceItems(nameOptionsList, checkOptionsList) { _, which, isChecked->
            checkOptionsList[which] = isChecked
        }

        builder.setPositiveButton(R.string.ok) { _, _ ->
            presenter.setConfigurationLocationFilterParams(checkOptionsList, filterType)
        }

        builder.create().show()

    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun setRecyclerNotResults() {
        layoutNotResults.visibility = View.VISIBLE
    }

    override fun restartModeSearch() {
        layoutNotResults.visibility = View.GONE
        updateAdapter(listOf<FlickrPhoto>())
    }

    override fun hideStartIndications() {
        layoutStartIndications.visibility = View.GONE
    }


}
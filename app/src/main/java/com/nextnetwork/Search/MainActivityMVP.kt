package com.nextnetwork.Search

import com.nextnetwork.Search.model.FilterParams
import com.nextnetwork.Search.model.FilterType
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto

interface MainActivityMVP {

    interface View {
        fun updateAdapter(listPhotos: List<FlickrPhoto>)
        fun intentToDetailActivity(itemInfo: FlickrPhoto?, putExtra: Boolean)
        fun setProgressBar(view: Int)
        fun collapseBtnSubmitText()
        fun showLocationFilterAlertDialog(checkOptionsList: BooleanArray, nameOptionsList: Array<String>, filterType: FilterType)
        fun showMessage(message: String)
        fun setRecyclerNotResults()
        fun restartModeSearch()
        fun hideStartIndications()

    }

    interface Presenter {
        fun btnFilterHasClicked(filterType: FilterType)
        fun onQueryTextSubmit(query: String)
        fun updateAdapter(listPhotos: MutableList<FlickrPhoto>, ishigherPagination: Boolean)
        fun itemRecyclerViewHasClicked(itemInfo: FlickrPhoto)
        fun createFilterAlertDialog(filterType: FilterType, filterParams: FilterParams)
        fun setConfigurationLocationFilterParams(checkOptionsList: BooleanArray, filterType: FilterType)
        fun failureDownload()
        fun requestError()

    }

    interface Model {
        fun onQueryTextSubmit(query: String)
        fun requestFilterParams(filterType: FilterType)
        fun setFilterParams(checkOptionsList: BooleanArray, filterType: FilterType)

    }

}
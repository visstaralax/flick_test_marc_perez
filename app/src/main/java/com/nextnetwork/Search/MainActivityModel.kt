package com.nextnetwork.Search

import com.nextnetwork.ApiModel.APIService
import com.nextnetwork.ApiModel.FlickMethodAPIService
import com.nextnetwork.ApiModel.FlickRetrofit
import com.nextnetwork.ApiModel.FlickrGetMethod
import com.nextnetwork.ApiModel.FlickrPhotoResponse.FlickrStateResponse
import com.nextnetwork.Search.model.FilterParams
import com.nextnetwork.Search.model.FilterType
import com.nextnetwork.Search.model.LocationParam
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import root.Root

class MainActivityModel internal constructor (val presenter: MainActivityMVP.Presenter) : MainActivityMVP.Model  {

    private var filterParams = FilterParams()
    private val flickrMethods = FlickrGetMethod()
    private lateinit var api: APIService
    private var lastQuery = ""

    init {
        fakeLocationDownload()
        setAPIService()
    }

    private fun fakeLocationDownload() {

        val param1 = LocationParam("Spain", true)
        val param2 = LocationParam("Japan", false)

        filterParams.listLocationParam.add(param1)
        filterParams.listLocationParam.add(param2)
    }

    private fun setAPIService() {
        api = FlickRetrofit.getAPIService()
    }

    override fun onQueryTextSubmit(query: String) {

        lastQuery = query
        val method = flickrMethods.getMethod(FlickMethodAPIService.SEARCH)
        val request = "${Root.appInfo.urlBase}services/rest/?method=$method&api_key=${Root.appInfo.flickrKey}&location=&text=$query&format=json&nojsoncallback=1"

        val call = api.getStateResponse(request)!!

        call.enqueue(object : Callback<FlickrStateResponse?> {
            override fun onResponse(
                call: Call<FlickrStateResponse?>,
                response: Response<FlickrStateResponse?>) {

                if (response.body() != null)
                    if (response.body()!!.status == "ok") {
                        val pageResponse = response.body()!!.images
                        val listPhotos = pageResponse.photo as MutableList<FlickrPhoto>

                        presenter.updateAdapter(listPhotos, pageResponse.ishigherPagination())

                    } else {
                        presenter.requestError()
                    }
            }

            override fun onFailure(call: Call<FlickrStateResponse?>, t: Throwable) {
               presenter.failureDownload()
            }

        })
    }

    override fun requestFilterParams(filterType: FilterType) {
        presenter.createFilterAlertDialog(filterType, filterParams)
    }

    override fun setFilterParams(checkOptionsList: BooleanArray, filterType: FilterType) {

       when (filterType) {
             FilterType.LOCATION -> filterParams.setNewLocationsParams(checkOptionsList) }

            if (lastQuery.isNotEmpty())
                onQueryTextSubmit(lastQuery)


       }



}
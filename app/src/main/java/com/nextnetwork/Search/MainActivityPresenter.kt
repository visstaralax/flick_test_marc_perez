package com.nextnetwork.Search

import Utils.Utils
import android.content.Context
import android.view.View
import com.nextnetwork.R
import com.nextnetwork.Search.model.FilterParams
import com.nextnetwork.Search.model.FilterType
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto

class MainActivityPresenter internal constructor(context: Context, val view: MainActivityMVP.View): MainActivityMVP.Presenter {

    val utils = Utils(context)
    val model = MainActivityModel(this)

    override fun btnFilterHasClicked(filterType: FilterType) {
        model.requestFilterParams(filterType)
    }

    override fun onQueryTextSubmit(query: String) {
        if (query.isNotEmpty()) {
            model.onQueryTextSubmit(query)
            view.hideStartIndications()
            view.setProgressBar(View.VISIBLE)
            view.restartModeSearch()
        }

    }

    override fun updateAdapter(listPhotos: MutableList<FlickrPhoto>, ishigherPagination: Boolean) {

        //si hay más paginación añadimos un item extra en la lista para notificarlo
        //si el ultimo item ya tiene una noti, la eliminamos para poder añadir más a continuación

        if (listPhotos.size > 0) {

            val lastItem = listPhotos.lastIndex

            if (listPhotos[lastItem].isOnlyNotificator)
                listPhotos.removeAt(lastItem)

            if (ishigherPagination) {
                val info = FlickrPhoto()
                val notification = utils.getString(R.string.more_results)
                info.setBtnMoreResults(notification)
                listPhotos.add(info)
            }
        }

        if (listPhotos.isEmpty()) {
            view.showMessage(utils.getString(R.string.not_results))
            view.setRecyclerNotResults()
        } else {
            view.updateAdapter(listPhotos)
        }

        view.setProgressBar(View.GONE)
    }

    override fun itemRecyclerViewHasClicked(itemInfo: FlickrPhoto) {

        if (!itemInfo.isOnlyNotificator)
            view.intentToDetailActivity(itemInfo, true)
    }

    override fun createFilterAlertDialog(filterType: FilterType, filterParams: FilterParams) {

        when (filterType) {
            FilterType.LOCATION -> {

                val checkOptionsList = mutableListOf<Boolean>()
                val nameOptionsList = mutableListOf<String>()
                val max = filterParams.listLocationParam.size

                for (i in 0 until max) {
                    checkOptionsList.add(filterParams.listLocationParam[i].isAdd)
                    nameOptionsList.add(filterParams.listLocationParam[i].location)
                }

                if (max == 0)
                    view.showMessage(utils.getString(R.string.not_avaible))
                else
                    view.showLocationFilterAlertDialog(checkOptionsList.toBooleanArray(), nameOptionsList.toTypedArray(), filterType)
            }
            else -> {view.showMessage(utils.getString(R.string.not_avaible))}
        }
    }

    override fun setConfigurationLocationFilterParams(checkOptionsList: BooleanArray, filterType: FilterType) {
        model.setFilterParams(checkOptionsList, filterType)
    }

    override fun failureDownload() {
        view.showMessage(utils.getString(R.string.try_again))
    }

    override fun requestError() {
        view.showMessage(utils.getString(R.string.request_fail))
    }

}
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.nextnetwork.ApiModel.FlickrMethodGetPhoto
import com.nextnetwork.ApiModel.RequestFlickrMethodPhoto
import com.nextnetwork.R
import com.nextnetwork.Search.MainActivityMVP
import com.nextnetwork.testflickr_marcperez.searchActivity.model.FlickrPhoto

class SearchAdapter internal constructor(val presenter: MainActivityMVP.Presenter) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private var flickrPhotoList: List<FlickrPhoto> = mutableListOf()
    private val flickrUrl = RequestFlickrMethodPhoto()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var titleText: TextView = view.findViewById(R.id.title)
        var authorText: TextView = view.findViewById(R.id.user)
        var simpleDraweeView: SimpleDraweeView = view.findViewById(R.id.img)
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flickr, viewGroup, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val flickrPhoto = flickrPhotoList[position]

        viewHolder.titleText.text = flickrPhoto.title
        viewHolder.authorText.text = flickrPhoto.owner

        val dir = flickrUrl.requestBase(FlickrMethodGetPhoto.ICON, flickrPhoto)
        viewHolder.simpleDraweeView.setImageURI(dir)

        viewHolder.itemView.setOnClickListener {
            presenter.itemRecyclerViewHasClicked(flickrPhoto)
        }
    }

    override fun getItemCount() = flickrPhotoList.size

    fun updateList (newList: List<FlickrPhoto>) {

        flickrPhotoList = newList
        notifyDataSetChanged()
    }


}

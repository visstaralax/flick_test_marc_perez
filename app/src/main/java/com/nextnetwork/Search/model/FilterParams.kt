package com.nextnetwork.Search.model

class FilterParams {

    val listLocationParam = mutableListOf<LocationParam>()

    fun setNewLocationsParams(checkOptionsList: BooleanArray) {
        for (i in checkOptionsList.indices) {
            listLocationParam[i].isAdd = checkOptionsList[i]
        }
    }

    fun getLocationsParamsToString(): String {

        var locations = ""

        for (i in listLocationParam.indices) {
            if (listLocationParam[i].isAdd)
                locations = locations + listLocationParam[i].location
        }

        return locations
    }
}
package com.nextnetwork.Search.model

enum class FilterType {
    SORT,
    DATE,
    TAG,
    LOCATION
}
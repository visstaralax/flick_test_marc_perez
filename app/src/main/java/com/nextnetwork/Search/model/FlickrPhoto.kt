package com.nextnetwork.testflickr_marcperez.searchActivity.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName;
import com.nextnetwork.R
import java.io.Serializable

class FlickrPhoto : Serializable
{
    @SerializedName("id") var id: String = ""
    @SerializedName("isfamily") var isfamily: Int = 0
    @SerializedName("isfriend") var isfriend: Int = 0
    @SerializedName("ispublic") var ispublic: Int = 0
    @SerializedName("title") var title: String = ""
    @SerializedName("farm") var farm: Int = 0
    @SerializedName("server") var server: String = ""
    @SerializedName("secret") var secret: String = ""
    @SerializedName("owner") var owner: String = ""

    //sirve para indicar que es un item notificador de info, y no una foto
    var isOnlyNotificator = false

    var date = ""
    var description = ""
    var userName = ""

    fun setBtnMoreResults(notification: String) {
        title = notification
        isOnlyNotificator = true
    }


}
package root

import Utils.Utils
import android.content.Context
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.nextnetwork.R

abstract class FlickrActivity : AppCompatActivity() {

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                onBackPressed()
                return true
            }
        }
        return false
    }

    protected fun setToolbar(view: Int, title: Int, btnBack: Boolean) {
        val toolbar: Toolbar = findViewById(view)
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.title = getString(title)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(btnBack);
        supportActionBar!!.setDisplayShowHomeEnabled(btnBack);
    }

    protected fun checkInternetConnection(context: Context): Boolean {
        val utils = Utils(context)
        return (utils.isNetworkAvailable())
    }
}

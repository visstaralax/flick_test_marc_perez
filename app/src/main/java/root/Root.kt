package root

import androidx.multidex.MultiDexApplication
import com.facebook.drawee.backends.pipeline.Fresco

class Root : MultiDexApplication() {

    companion object {
        val appInfo = AppInfo()
    }

    override fun onCreate() {
        super.onCreate()

        setAppInfo()
        initFrescoPlugin()
    }

    private fun initFrescoPlugin() {
        Fresco.initialize(this)
    }

    private fun setAppInfo() {
        appInfo.flickrKey = "98680ad2af8857f03eb2eadb3f6a4d7b"
        appInfo.secretKey = "6a5856462f127b81"
        appInfo.user = ""
        appInfo.urlBase = "https://www.flickr.com/"
    }

}